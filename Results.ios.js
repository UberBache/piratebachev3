/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

var REQUEST_URL = 'https://node-pirate.herokuapp.com/search/';


'use strict';
import React, {
  AppRegistry,
  ActivityIndicatorIOS,
  Component,
  StyleSheet,
  Text,
  ListView,
  View,
  AlertIOS,
  TouchableHighlight
} from 'react-native';

class Results extends Component {


  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
  	console.log(REQUEST_URL);
  	console.log(this.props.data);
  	var query = this.props.data;
  	var search = REQUEST_URL + query;
  	console.log(search);
    this.props.data = "";



    fetch(search)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData),
          loaded: true,
        });
      })
      .done();
  }

	downloadTorrent(torrent) {
	    var base_url = "https://galley-slave.herokuapp.com/torrent?data[magnet]=";
	    var magnet = torrent.magnetLink
	    var url = base_url.concat(magnet);
	    console.log(url)

	    fetch(url)
	      .then(function(response) {
	        return response.json()
	      }).then(function(json) {
	        console.log('parsed json', json)
	      }).catch(function(ex) {
	        console.log('parsing failed', ex)
	      })


	    AlertIOS.alert('Alert', 'Torrent Downloaded to Boxee!');

	}  



  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderTorrent.bind(this)}
        style={styles.listView}
      />
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.loadingContainer}>
          <ActivityIndicatorIOS
          size='large'/>
          <Text style={styles.loadingText}>
            Loading torrents...
          </Text>
      </View>
    );
  }

  renderTorrent(torrent) {
    return (
    <TouchableHighlight onPress={() => this.downloadTorrent(torrent)}>    	
      <View>
        <View style={styles.container}>
          <View style={styles.insideContainer}>
            <Text style={styles.title}>{torrent.name}</Text>
          </View>
        </View>
        <Text style={styles.seeds}>Seeds: {torrent.seeders}  Leechers: {torrent.leechers}  Size: {torrent.size}</Text>
        <View style={styles.separator} /> 
      </View>
    </TouchableHighlight>
    );
  }

}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 18
  },
  insideContainer: {
    flex: 1,
  },
  loadingContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingText: {
    fontSize: 18,
    paddingTop: 25
  },
  title: {
    fontSize: 16,
    textAlign: 'center',
  },
  listView: {
    backgroundColor: '#F5FCFF',
    paddingTop: 60
  },
  separator: {
        height: 1,
        backgroundColor: '#dddddd'
    },  
    seeds: {
        color: '#656565',
        fontSize: 12,
    },
    leeches: {
        color: '#656565',
        width: 150,
        fontSize: 12,
    },
    size: {
        color: '#656565',
        width: 150,
        fontSize: 12,
    },    
});

module.exports = Results;
