
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  WebView
} from 'react-native';

var global = 'http://23.233.67.75:9091/transmission/web/';

class Torrents extends Component {
  render() {
    return (
      <View style={styles.container}>
        <WebView source={{uri: global}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

module.exports = Torrents;
