/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

var REQUEST_URL = 'https://node-pirate.herokuapp.com/tv';



'use strict';
import React, {
  AppRegistry,
  ActivityIndicatorIOS,
  Component,
  StyleSheet,
  Text,
  ListView,
  View
} from 'react-native';

class TVPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData),
          loaded: true,
        });
      })
      .done();
  }

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderShow}
        style={styles.listView}
      />
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.loadingContainer}>
          <ActivityIndicatorIOS
          size='large'/>
          <Text style={styles.loadingText}>
            Loading shows...
          </Text>
      </View>
    );
  }

  renderShow(show) {
    return (
      <View>
        <View style={styles.container}>
          <View style={styles.insideContainer}>
            <Text style={styles.title}>{show.name}</Text>
          </View>         
        </View>
        <View style={styles.separator} /> 
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 18
  },
  insideContainer: {
    flex: 1,
  },
  loadingContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingText: {
    fontSize: 18,
    paddingTop: 25
  },
  title: {
    fontSize: 16,
    textAlign: 'center',
  },
  listView: {
    backgroundColor: '#F5FCFF',
  },
  separator: {
        height: 1,
        backgroundColor: '#dddddd'
    },  
});

module.exports = TVPage;
