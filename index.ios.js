/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

var Home = require('./Home');
var Torrents = require('./Torrents');
var Search = require('./search');

'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  NavigatorIOS,
  View
} from 'react-native';


class piratebache extends Component {

  render() {
    return (
      <NavigatorIOS
      ref="nav"
          style={styles.nav}
            barTintColor='#000'
            titleTextColor='#fff'
            initialRoute={{
                component: Home,
                title: "Pirate Bache",
                leftButtonTitle: 'Torrents',
                onLeftButtonPress: () => {
                  this.refs.nav.navigator.push({
                    title: "Downloads",
                    component: Torrents,
                    leftButtonTitle: 'Back',
                    onLeftButtonPress: () => { this.refs.nav.navigator.pop(); }
                  });},
                rightButtonTitle: 'Search',
                onRightButtonPress: () => {
                  this.refs.nav.navigator.push({
                    title: "Search",
                    component: Search,
                    leftButtonTitle: 'Back',
                    onLeftButtonPress: () => { this.refs.nav.navigator.pop(); }
                  });}

              }}
        />
    );
  }

}

const styles = StyleSheet.create({

  nav: {
    flex: 1
  },
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


AppRegistry.registerComponent('piratebache', () => piratebache);
