/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

var ScrollableTabView = require('react-native-scrollable-tab-view');
var MoviePage = require('./MoviePage');
var TvPage = require('./TvPage');



'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View
} from 'react-native';

class Home extends Component {
  render() {
    return (
      <ScrollableTabView style={styles.container}>
        <MoviePage tabLabel="Top Movies" />
        <TvPage tabLabel="Top TV Shows" />
      </ScrollableTabView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 70
  },
  welcome: {
    fontSize: 50,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

module.exports = Home;
