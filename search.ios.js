
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight
} from 'react-native';

var Results = require('./Results');
var FloatLabelTextInput = require('react-native-floating-label-text-input');
var SearchBar = require('react-native-search-bar');

var searchText = "";

class Search extends Component {

constructor(props) {
    super(props); 
      this.state = {
        searchText: '',
      };
  }

  onType(value) {
      this.state.searchText = value;
      console.log(searchText)
  }

  onPress() {     
    console.log(this.state.searchText);
    this.props.navigator.push({
      title: "Results",
      component: Results,
      passProps: {data: this.state.searchText}
    });
    
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.instructions}>Search for Movie or TV Series</Text>
       
        <SearchBar
            ref='searchBar'
            placeholder='Search'
            onChangeText={this.onType.bind(this)}
            onSearchButtonPress={this.onPress.bind(this)}
            />              
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 5,
        height: 650,
        padding: 10,
        backgroundColor: 'white',
        transform: [{'translate':[0,0,1]}] 
    },
    searchInput: {
        height: 36,
        marginTop: 10,
        marginBottom: 10,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#0033CC',
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        paddingLeft: 5
    },
    button: {
        height: 36,
        backgroundColor: '#0033CC',
        borderColor: 'navy',
        borderWidth: 1,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    buttonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white',
        alignSelf: 'center'
    },
    instructions: {
        fontSize: 18,
        alignSelf: 'center',
        marginBottom: 15
    },
    fieldLabel: {
        fontSize: 15,
        marginTop: 15
    },
    errorMessage: {
        fontSize: 15,
        alignSelf: 'center',
        marginTop: 15,
        color: 'red'
    },
    centering: {
      marginTop: -35,
      marginLeft: 130,
      alignItems: 'center',
      justifyContent: 'center',
    }
});

module.exports = Search;
